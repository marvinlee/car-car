import React from "react";
import { Link } from 'react-router-dom';

class ManufacturerList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      manufacturers: []
    }
  };

  async componentDidMount() {
    const url = '	http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({manufacturers: data.manufacturers});
    }
  }

  render() {
    return (
      <>
      <div className="px-4 py-4 my-2">
        <div className="col-lg-6 mx-auto text-center">
          <h1 className="display-5 fw-bold">Manufacturers</h1>
        </div>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-3">
              <Link to="/manufacturers/new" className="btn btn-dark btn-lg px-4 gap-3">Create</Link>
        </div>
        <table className="table">
          <thead className="table-dark">
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {this.state.manufacturers.map(manufacturer => {
              return (
                <tr key={manufacturer.id}>
                  <td>{ manufacturer.name }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      </>
    );
  };
}

export default ManufacturerList;
