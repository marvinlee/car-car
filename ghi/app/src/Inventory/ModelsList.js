import React from "react";
import { Link } from 'react-router-dom';

class ModelsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      models: [],
    }
  };

  async componentDidMount() {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({
        models: data.models,
      });
    }
  }

  render() {
    return (
      <>
      <div className="px-4 py-4 my-2">
        <div className="col-lg-6 mx-auto text-center">
          <h1 className="display-5 fw-bold">Models</h1>
        </div>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-3">
              <Link to="/models/new" className="btn btn-dark btn-lg px-4 gap-3">Create</Link>
        </div>
        <br></br>
        <div className="row">
        {this.state.models.map(model => {
            return (
              <div key={model.id} className="col-md-4">
                <div className="card mb-2 shadow">
                  <img src={model.picture_url} alt='model' className="card-img-top" style={{height: 200}} />
                    <div className="card-body">
                      <h4 className="card-title">Model: {model.name}</h4>
                      <h5 className="card-subtitle mb-2 text-muted">Manufacturer: {model.manufacturer.name} </h5>
                    </div>
                  </div>
                </div>
            );
          })}
        </div>

      </div>
      </>
    );
  };

}

export default ModelsList;
