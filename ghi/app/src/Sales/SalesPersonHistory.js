import React from 'react';

class SalesPersonHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
      salesperson: "",
      salesrecords: [],
      salespeople: [],
    }
  }

  async componentDidMount() {
    const salerecordsUrl = 'http://localhost:8090/api/salerecords/';
    const salespeopleUrl = 'http://localhost:8090/api/salesperson/';
    const salerecordsResponse = await fetch(salerecordsUrl)
    const salespeopleResponse = await fetch(salespeopleUrl);

    if (salerecordsResponse.ok && salespeopleResponse.ok) {
      const salerecordsData = await salerecordsResponse.json();
      const salespeopleData = await salespeopleResponse.json();
      this.setState({
        salesperson: "",
        salesrecords: salerecordsData.salerecord,
        salespeople: salespeopleData.salesperson,
      });
    }
  }

  handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    this.setState({[name]: value});
  }

  render () {
    return (
      <>
      <div className="px-4 py-4 my-2 text-center">
        <div className="col-lg-6 mx-auto text-center">
          <h1 className="display-5 fw-bold">Sales History</h1>
        </div>
        <div className="mb-3">
          <select onChange={this.handleChange} name="salesperson" id="salesperson" className="form-select" required value={this.state.salesperson}>
            <option value="">Choose a Sales Person</option>
            {this.state.salespeople.map(salesperson => {
              return (
                <option key={salesperson.id} value={salesperson.id}>{salesperson.name}</option>
              )
            })}
          </select>
        </div>
        <table className="table">
        <thead className="table-dark">
          <tr>
            <th>Sales Person</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Sale Price</th>
          </tr>
        </thead>
        <tbody>
          {this.state.salesrecords.filter(salerecord => salerecord.salesperson.id.toString() === this.state.salesperson).map(salerecord => {
            return (
              <tr key={salerecord.id}>
                <td>{ salerecord.salesperson.name }</td>
                <td>{ salerecord.customer.name }</td>
                <td>{ salerecord.automobile.vin }</td>
                <td>{ salerecord.price }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
      </>
    );
  }
}

export default SalesPersonHistory;
