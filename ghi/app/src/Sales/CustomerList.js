import React from "react";
import { Link } from 'react-router-dom';

class CustomerList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      customers: [],
    }
  };

  async componentDidMount() {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({customers: data.customer})
    }
  }

  render() {
    return (
      <div className="px-4 py-4 my-2 text-center">
        <div className="col-lg-6 mx-auto text-center">
          <h1 className="display-5 fw-bold">Customers</h1>
        </div>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-3">
              <Link to="/customers/new" className="btn btn-dark btn-lg px-4 gap-3">Create</Link>
        </div>
        <table className="table">
          <thead className="table-dark">
            <tr>
              <th>Name</th>
              <th>Phone Number</th>
            </tr>
          </thead>
          <tbody>
            {this.state.customers.map(customer => {
              return (
                <tr key={customer.id}>
                  <td>{ customer.name }</td>
                  <td>{ customer.number }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  };
}

export default CustomerList;
