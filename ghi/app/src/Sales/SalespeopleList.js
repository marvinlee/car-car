import React from 'react';
import { Link } from 'react-router-dom'

class SalespeopleList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      salespeople: [],
    }
  };

  async componentDidMount() {
    const url = 'http://localhost:8090/api/salesperson/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({salespeople: data.salesperson})
    }
  }

  render() {
    return (
      <div className="px-4 py-4 my-2 text-center">
        <div className="col-lg-6 mx-auto text-center">
          <h1 className="display-5 fw-bold">Salespeople</h1>
        </div>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-3">
              <Link to="/salespeople/new" className="btn btn-dark btn-lg px-4 gap-3">Create</Link>
        </div>
        <table className="table">
          <thead className="table-dark">
            <tr>
              <th>Name</th>
              <th>Employee Number</th>
            </tr>
          </thead>
          <tbody>
            {this.state.salespeople.map(salesperson => {
              return (
                <tr key={salesperson.id}>
                  <td>{ salesperson.name }</td>
                  <td>{ salesperson.number }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  };







}

export default SalespeopleList;
