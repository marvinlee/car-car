import React from "react";
import { Link } from 'react-router-dom';

class SaleRecordList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      salerecords: []
    }
  };

  async componentDidMount() {
    const url = 'http://localhost:8090/api/salerecords/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json()
      this.setState({salerecords: data.salerecord});
    }
  }

  render() {
    return (
      <div className="px-4 py-4 my-2 text-center">
        <div className="col-lg-6 mx-auto text-center">
          <h1 className="display-5 fw-bold">Sales Records</h1>
        </div>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-3">
              <Link to="/salerecords/new" className="btn btn-dark btn-lg px-4 gap-3">Create</Link>
        </div>
        <table className="table">
          <thead className="table-dark">
            <tr>
              <th>Sales Person</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.salerecords.map(salerecord => {
              return (
                <tr key={salerecord.id}>
                  <td>{ salerecord.salesperson.name }</td>
                  <td>{ salerecord.customer.name }</td>
                  <td>{ salerecord.automobile.vin }</td>
                  <td>{ salerecord.price }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  };
}

export default SaleRecordList;
