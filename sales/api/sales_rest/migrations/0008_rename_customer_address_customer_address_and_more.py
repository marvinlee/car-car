# Generated by Django 4.0.3 on 2022-10-28 19:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0007_alter_salerecord_automobile_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customer',
            old_name='customer_address',
            new_name='address',
        ),
        migrations.RenameField(
            model_name='customer',
            old_name='customer_name',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='customer',
            old_name='phone_number',
            new_name='number',
        ),
    ]
