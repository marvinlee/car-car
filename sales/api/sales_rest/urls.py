from django.urls import path
from .views import api_salesperson, api_customer, api_sale_record

urlpatterns = [
    path("salesperson/", api_salesperson, name="api_create_salesperson"),
    path("customers/", api_customer, name="api_create_customer"),
    path("salerecords/", api_sale_record, name="api_create_sale_record"),
]
