from common.json import ModelEncoder
from .models import Appointment, AutomobileVO, Technician


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href",]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "appointment_time",
        "appointment_details",
        "technician",
        "is_completed",
        "vip",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["technician_name", "employee_number"]
