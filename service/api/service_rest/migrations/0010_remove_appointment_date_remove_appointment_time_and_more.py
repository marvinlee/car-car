# Generated by Django 4.0.3 on 2022-10-26 17:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0009_remove_appointment_appointment_time_appointment_date_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appointment',
            name='date',
        ),
        migrations.RemoveField(
            model_name='appointment',
            name='time',
        ),
        migrations.AddField(
            model_name='appointment',
            name='appointment_time',
            field=models.DateTimeField(null=True),
        ),
    ]
